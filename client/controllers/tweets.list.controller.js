
cnnTweetApp.controller('TweetsListController', ['$scope', '$routeParams', 'Tweets', function($scope, $routeParams, Tweets) {

    var user = $routeParams.user || 'cnnbrk';

    var init = function() {

        Tweets.getUserTweets(user)
            .success(function(data) {

                var dataGroup = [];
                var count = data.length;

                if(data && count >= 10) {

                    var midPoint = Math.round(data.length/2);

                    dataGroup.push(data.slice(0, midPoint));
                    dataGroup.push(data.slice(midPoint, count));

                } else {

                    dataGroup.push(data);
                }


                $scope.tweets = dataGroup;
            })
            .error(function(err) {

                console.log(err);
            });
    };


    init();

}]);