
'use strict'

// App Module

var cnnTweetApp  = angular.module('cnnTweetApp', [
    'ngRoute',
    'ngSanitize'
]);

cnnTweetApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/:user', {

            templateUrl: '/views/tweets.list.view.html',
            controller: 'TweetsListController'
        })
        .otherwise({
            redirectTo: '/cnnbrk'
        })
}]);

window.cnnTweetApp = cnnTweetApp;