cnnTweetApp.factory('Tweets', ['$http', function($http) {

    var getUserTweets = function(user) {

        var url = '/api/tweets/' + user;

        return $http.get(url);

    };

    return {
        getUserTweets: getUserTweets
    }

}]);