
/**
 * Module dependencies.
 */

var express = require('express'),
    http = require('http'),
    path = require('path'),
    dust = require('dustjs-linkedin'),
    cons = require('consolidate'),
    middleware = require('./lib/middleware');
    appController = require('./controllers');


// Configuration properties
var app = express();

var port = process.env.PORT || 3000;
var viewsDir =  path.join(__dirname, '/views');
var appDir = path.join(__dirname, '../client');

// Express app configurations

app.engine('dust', cons.dust);

app.configure(function () {
    app.set('port', port);
    app.set('views', viewsDir);
    app.set('view engine', 'dust');

    app.use(express.compress());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(middleware.queryFilter());
    app.use(middleware.cors());

    app.use(express.static(appDir));
    app.use(app.router);
    app.use(middleware.notFound());
});


// Initializing the app controller.
appController.initialize(app);

// Listen on server application
http.createServer(app).listen(port, function() {
    console.log('Cool CNN Tweet server listening on port ' + port);
});
