'use strict'

var Q = require('q'),
    _ = require('underscore'),
    config = require('../config'),
    twText = require('twitter-text'),
    Twitter = require('twitter-js-client').Twitter;

var Tweets = function() {

    this._twitter = new Twitter(config.apis.twitter);
};

Tweets.prototype = {

    _twitter: null,

    getUserTweets: function(params) {

        var deferred = Q.defer();

        this._twitter.getUserTimeline({ screen_name: params.user, count: params.limit, exclude_replies: true},

            // Error Callback
            function onError(err) {

                deferred.reject(err);
            },

            // Success Callback
            function onSuccess(data) {

                var result = [];

                if(data) {

                    data = JSON.parse(data);

                    _.each(data, function(item) {

                        result.push({
                            id: item.id,
                            text: twText.autoLink(twText.htmlEscape(item.text), {usernameUrlBase: '/'}),
                            entities: item.entities,
                            user: _.pick(item.user, 'id', 'name', 'screen_name', 'profile_image_url')
                        });
                    });
                }


                deferred.resolve(result);
            }
        );

        return deferred.promise;
    }
};

module.exports = new Tweets();