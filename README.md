# CNN Tweet App
Just a cool app. Really cool. I mean really cool. :)

## Navigation
You can navigate by using a users twitter handler.

- [http://localhost:30000/cnnbrk][2]
- [http://localhost:30000/cnn][2]
- [http://localhost:30000/ejferg][2]

You can also use the link below. (pulls data from @cnnbrk).

- [http://localhost:30000/cnnbrk-tweets][2]

Clicking a users twitter handler within a link will show that users data on the page.

## Run app locally
npm install

node server/app.js
